#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "date.h"
#include "habit.h"
#include "config.h"
#include "constants.h"

int
add_habit(struct habit_tracker *tracker, char *name, int stati)
{
	struct habit *first = tracker->first;
	struct habit *new_habit;
	new_habit = malloc(sizeof(struct habit));
	if (new_habit == NULL) {
		return -1;
	}
	new_habit->stati = stati;
	new_habit->name = strdup(name);
	new_habit->next = first;

	tracker->first = new_habit;
	return 0;
}

int
habit_from_file(struct habit_tracker *tracker, FILE *fp)
{
	char linebuf[HABIT_NAME_SIZE + 8];
	char date[DATE_SIZE];
	int stati;
	char name[HABIT_NAME_SIZE];
	int first = 1;

	tracker->first = NULL;
	tracker->size = 0;
	while(fgets(linebuf, HABIT_NAME_SIZE, fp)) {
		if (first) {
			sscanf(linebuf, "%s\n", &date);
			tracker->date = strdup(date);
			first = 0;
			continue;
		}
		sscanf(linebuf, "%d\t%[^\t\n]", &stati, &name);
		add_habit(tracker, name, stati);
		++(tracker->size);
	}
	return 0;
}

void
print_status(int status)
{
	if (status) {
		printf(COLOR_PREFIX, DONE_COLOR);
		printf(DONE_CHAR);
	} else {
		printf(COLOR_PREFIX, FAIL_COLOR);
		printf(FAIL_CHAR);
	}
	printf(COLOR_SUFFIX);
}

void
print_habit(int habit)
{
	for (int i = 0; i < TRACK_LENGTH; ++i) {
		int status = habit % 2;
		print_status(status);
		habit >>= 1;
	}
}

void
print_habits(struct habit_tracker *tracker)
{
	struct habit *current = tracker->first;
	printf("%s\n", tracker->date);
	print_days();

	for (int i = 0; i < tracker->size && current != NULL; ++i) {
		print_habit(current->stati);
		printf(" (%d) %s\n", i, current->name);
		current = current->next;
	}
}
