#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "date.h"
#include "config.h"

int
get_day_diff(struct tm *date)
{
	time_t now;
	time(&now);
	double sec_diff = difftime(now, mktime(date));
	int day_diff = sec_diff / 3600 / 24;
	return day_diff;
}

struct tm
parse_date(char *date_str)
{
	char year[4], month[3], day[3];
	int i_year, i_mon, i_day;
	sscanf(date_str, "%04s-%02s-%02s", year, month, day);
	i_year = atoi(year) - 1900;
	i_mon = atoi(month) - 1;
	i_day = atoi(day);
	struct tm date = {.tm_year=i_year, .tm_mon=i_mon, .tm_mday=i_day};
	mktime(&date);
	return date;
}

void
print_days()
{
	char day[3];
	time_t current_time;
	current_time = time(NULL);
	struct tm *local = localtime(&current_time);
	for (int i = TRACK_LENGTH; i >0; --i) {
		local->tm_mday -= i;
		mktime(local);
		strftime(day, sizeof day, DAY_REPR, local);
		printf("%s ", day);
		local->tm_mday += i;
	}
	printf("\n");
}
