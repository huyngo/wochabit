#ifndef DATE_H
#define DATE_H

int get_day_diff(struct tm *date);
struct tm parse_date(char *date_str);
void print_days();

#endif
