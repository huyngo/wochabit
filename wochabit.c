/** Main file for Wochabit
  * Copyright (C) 2021 Ngô Ngọc Đức Huy
  *
  * This file is part of Wochabit.
  *
  * Wochabit is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * Wochabit is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with Wochabit.  If not, see <https://www.gnu.org/licenses/>.
  * */
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "constants.h"
#include "config.h"
#include "date.h"
#include "habit.h"


int
main(int argc, char *argv[]){
	struct habit_tracker tracker;
	FILE *fp;
	char *fname;
	fname = malloc(strlen(getenv("HOME")) + strlen(HABIT_FILE) + 1);
	strcpy(fname, getenv("HOME"));
	strcat(fname, HABIT_FILE);
	fp = fopen(fname, "r");
	habit_from_file(&tracker, fp);
	fclose(fp);
	print_habits(&tracker);
	return 0;
}
