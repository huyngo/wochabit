#ifndef HABIT_H
#define HABIT_H

struct habit {
	int stati; // Plural of status :)
	char *name;
	struct habit *prev;
	struct habit *next;
};

struct habit_tracker {
	char *date;
	int size;
	struct habit *first;
};

int add_habit(struct habit_tracker *tracker, char *name, int stati);
int del_habit(struct habit_tracker *tracker, int position);
int habit_from_file(struct habit_tracker *tracker, FILE *fp);

void print_status(int status); // Print the status for a day of a habit
void print_habit(int habit); // Print the habit stati during the tracked duration
void print_habits(struct habit_tracker *tracker);
#endif
