#ifndef CONFIG_H
#define CONFIG_H

#include "constants.h"

static const enum color DONE_COLOR = GREEN;
static const enum color FAIL_COLOR = RED;

// The way the day is represented in each column
// Recommended values (see strftime(3):
// %a: day of the week
// %d: day of month
static const char DAY_REPR[] = "%d";
static const int REPR_LENGTH = 3;

// Default is a full block,
// SHOULD be 1 character longer than the length of DAY_REPR
static const char DONE_CHAR[] = "\u2588\u2588\u2588";
static const char FAIL_CHAR[] = "\u2588\u2588\u2588";

// The number of days for habit tracking
static const int TRACK_LENGTH = 7;
static const char HABIT_FILE[] = "/.wochabit";

#endif
