all: generate wochabit clean

generate:
	@echo "Generating binaries"
	gcc -Wall -c date.c
	gcc -Wall -c habit.c
	gcc -Wall -c wochabit.c

wochabit:
	@echo "Building executable"
	mkdir -p build
	gcc -o build/wochabit date.o habit.o wochabit.o

clean:
	@echo "Cleaning up..."
	rm *.o
