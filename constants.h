#ifndef CONSTANTS_H
#define CONSTANTS_H

enum color{
	BLACK,
	RED,
	GREEN,
	YELLOW,
	BLUE,
	MAGENTA,
	CYAN,
	WHITE
};

static const char COLOR_PREFIX[] = "\033[3%d;1m";
static const char COLOR_SUFFIX[] = "\033[0m";

static const int DATE_SIZE = 11; // "YYYY-MM-DD\0" = 11 chars
static const int HABIT_NAME_SIZE = 64; // A habit name shouldn't be that long

#endif
